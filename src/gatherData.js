'use strict';

const VISITED = 'true';
const VISITED_ATTR = 'visited';

/**
 * Сбор данных со страницы
 */
export class GatherData
{
    constructor (options) {
        this.parameterClass = options.parameterClass || 'item';
        this.componentClass = options.componentClass || 'component';
        this.workspace = options.workspace || 'service';
        this.isJson = options.isJson || false;

        /**
         * Обработчики для компонентов
         * @type {{}}
         */
        this.handlers = {};

        /**
         * Возвращает обработчик компонента или null
         * @param componentName
         * @returns {*}
         */
        this.getHandler = function (componentName) {
            if (this.handlers.hasOwnProperty(componentName)) {
                return this.handlers[componentName];
            }

            return null;
        };

        /**
         * Стандартный обработсик параметров
         * @param tool
         */
        this.parameterHandler = function (tool) {
            let handler = this.getHandler(tool.dataset.type);
            if (handler === null) {
                return tool.value;
            }

            return handler(tool);
        };

        /**
         * Обработчик компонентов
         * @param component
         */
        this.componentHandler = function (component) {
            let handler = this.getHandler(component.dataset.type);
            if (handler === null) {
                return this.getData(component);
            }

            return handler(component);
        };

        /**
         * Получение класса параметров
         * @returns {*}
         */
        this.getParameterClass = function() {
            return this.parameterClass;
        };

        /**
         * Получение класса компонентов
         * @returns {*}
         */
        this.getComponentClass = function() {
            return this.componentClass;
        };

        /**
         * Возвращает рабочую область
         * @returns {*|string}
         */
        this.getWorkspace = function () {
            return this.workspace;
        };

        /**
         * Возвращает все элементы рабочей области
         * @returns {Array}
         */
        this.getChildrenElements = function (elements, findClass) {

            let children = null,
                element = null,
                data = [];

            if (elements instanceof HTMLElement) {
                elements = [
                    elements
                ];
            }

            for (let elementNum = 0; elementNum < elements.length; ++elementNum) {
                element = elements[elementNum];
                children = element.children;

                for (let childrenCount = 0; childrenCount < children.length; ++childrenCount) {
                    let childrenElement = children[childrenCount];

                    if (childrenElement.classList.contains(findClass)) {
                        data.push(childrenElement);
                    }
                }
            }

            return data;
        };

        /**
         * Создает обьект внутри обьекта
         */
        this.initDataLevel = function (data, name) {
            if (!data.hasOwnProperty(name)) {
                data[name] = {};
            }
        };

        this.afterGatherData = function() {
            let tools = document.getElementsByClassName(this.getParameterClass());

            for (let tool in tools) {
                if (tools.hasOwnProperty(tool)) {
                    tools[tool].removeAttribute(VISITED_ATTR);
                }
            }
        }
    }

    /**
     * Установка значения класса параметра
     * @param parameterClass
     * @returns {GatherData}
     */
    setParameterClass(parameterClass) {
        this.parameterClass = parameterClass;

        return this;
    }

    /**
     * Установка значения класса компонента
     * @param componentClass
     * @returns {GatherData}
     */
    setComponentClass(componentClass) {
        this.componentClass = componentClass;

        return this;
    }

    /**
     * Вывод {@see JSON}
     * @returns {GatherData}
     */
    asJson() {
        this.isJson = true;

        return this;
    }

    /**
     * Вывод {@see Object}
     * @returns {GatherData}
     */
    asObject() {
        this.isJson = false;

        return this;
    }

    /**
     * Добавление нового обработчика
     * @param elementName
     * @param elementHandler
     * @returns {GatherData}
     */
    addHandler(elementName, elementHandler) {
        this.handlers[elementName] = elementHandler;

        return this;
    }

    /**
     * Сбор данных
     */
    gatherData() {
        let data = this.getData(
            document.getElementsByClassName(
                this.getWorkspace()
            )
        );

        this.afterGatherData();

        if (this.isJson) {
            return JSON.stringify(data);
        }

        return data;
    }

    /**
     * Рекурсивный сбор данных
     * @param elements
     */
    getData(elements) {
        let children = this.getChildrenElements (
            elements,
            this.getComponentClass()
            ),
            gatheredData = {},
            childType = null,
            child = null,
            tools = null,
            tool = null,
            toolType = null,
            childData = null;

        for (let count = 0; count < children.length; ++count) {
            child = children[count];
            childType = child.dataset.type;
            childData =  this.componentHandler(child);

            if (Object.keys(childData).length !== 0) {
                this.initDataLevel(
                    gatheredData,
                    childType
                );

                this.initDataLevel(
                    gatheredData[childType],
                    count
                );

                gatheredData[childType][count] = childData;
            }

            tools = child.getElementsByClassName(this.getParameterClass());

            for (let toolsCount = 0; toolsCount < tools.length; ++toolsCount) {
                tool = tools[toolsCount];
                toolType = tool.dataset.type;

                if (tool.getAttribute(VISITED_ATTR) !== VISITED) {
                    tool.setAttribute(VISITED_ATTR, VISITED);

                    this.initDataLevel(
                        gatheredData,
                        childType
                    );

                    this.initDataLevel(
                        gatheredData[childType],
                        count
                    );

                    this.initDataLevel(
                        gatheredData[childType][count],
                        toolType
                    );

                    gatheredData[childType][count][toolType][toolsCount] = this.parameterHandler(tool);
                }
            }
        }

        return gatheredData;
    }
}
